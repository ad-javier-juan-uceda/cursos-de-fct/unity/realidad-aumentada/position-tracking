﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class tocarVampiro : MonoBehaviour
{
    public Transform jugador;
    public float tamanorandom;
    public int puntos;
    public Text txtpuntos;
    // Start is called before the first frame update
    void Start()
    {
        cambiarPos();
        txtpuntos.text = puntos.ToString();
    }
    private void OnMouseDown()
    {
        cambiarPos();
        sumarPos();
    }
    
    private void sumarPos()
    {
        puntos++;
        txtpuntos.text = puntos.ToString();
    }
    
    private void cambiarPos()
    {
        transform.position = new Vector3((Random.insideUnitSphere.x * tamanorandom), transform.position.y ,(Random.insideUnitSphere.z * tamanorandom));
        Vector3 posJugador= new Vector3(jugador.position.x, transform.position.y ,jugador.position.z);
        transform.LookAt(posJugador);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
